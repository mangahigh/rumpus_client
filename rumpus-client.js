(function(f) {
    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
        module.exports = f();

        // RequireJS
    } else if (typeof define === "function" && define.amd) {
        define([], f);

        // <script>
    } else {
        var g
        if (typeof window !== "undefined") {
            g = window;
        } else if (typeof global !== "undefined") {
            g = global;
        } else if (typeof self !== "undefined") {
            g = self;
        } else {
            g = this;
        }

        g.RumpusClient = f();
    }
})(function () {
    /**
     * @type object
     */
    RumpusClient.MESSAGES = {
        USER_PROPS: 'user-properties',
        LOBBY_USERS: 'lobby-users',
        USER_JOIN: 'user-join',
        USER_LEAVE: 'user-leave',
        USER_CONNECT: 'user-connect',
        USER_DISCONNECT: 'user-disconnect',
        ROOM_EMPTY: 'room-empty',
        ROOM_FULL: 'room-full',
        ERROR: 'error',
        RECONNECT_ERROR: 'reconnect-error'

    };

    /**
     * @param opts
     * @constructor
     */
    function RumpusClient(opts) {
        this.opts = opts || {};
        this.serverUrl = this.opts.serverUrl;
        this.io = this.opts.io;
        this.user = this.opts.user;
        this.phaser = this.opts.phaser || Phaser;
    }

    /** @private */
    function onConnect() {
        this.socket.emit(RumpusClient.MESSAGES.USER_PROPS, this.user);
    };

    /** @private */
    function onError(err) {
        console.error(err);
    };

    /** @private */
    function exposeDefaultSignals() {
        var key, signal, signalName;
        for (var i = 0; i < Object.keys(RumpusClient.MESSAGES).length; i++) {
            key = Object.keys(RumpusClient.MESSAGES)[i];
            signal = getSignal.call(this);
            signalName = camelize(RumpusClient.MESSAGES[key]);
            this[signalName] = signal;
            this.listenTo(key, signal.dispatch, this)
        }
    };

    /** @private */
    function getSignal() {
        return (new this.phaser.Signal());
    };

    /** @private */
    function camelize(str) {
        return str.replace(/\-([a-z])/g, function(letter) {
            return letter.replace('-', '').toUpperCase();
        }).replace(/\s+/g, '');
    };

    /** @private */
    function attachDefaultHandlers() {
        this.listenTo(RumpusClient.MESSAGES.USER_CONNECT, onConnect, this);
        this.listenTo(RumpusClient.MESSAGES.ERROR, onError, this);
        this.listenTo(RumpusClient.MESSAGES.RECONNECT_ERROR, onError, this);
        exposeDefaultSignals.call(this);
    }

    /**
     * Connects to configured rumpus socket, and attaches
     * default handlers for standard rumpus messages
     */
    RumpusClient.prototype.connect = function () {
        if (!this.socket) {
            this.socket = this.io(this.serverUrl, this.opts.server);
            attachDefaultHandlers.call(this);
        }
        else {
            this.socket.connect();
        }
    };

    /**
     * Disconnects from rumpus socket
     */
    RumpusClient.prototype.disconnect = function () {
        this.socket.disconnect();
    };

    /**
     * Proxies through to socket.on, binding to correct context
     *
     * @param {string} message
     * @param {function} handler
     * @param {object} context
     */
    RumpusClient.prototype.listenTo = function (message, handler, context) {
        this.socket.on(message, (function (data) {
            handler(data);
        }).bind(context));
    };

    return RumpusClient;
});