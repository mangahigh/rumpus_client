# Rumpus Client

## Installation

To run tests:

```sh
npm install;
karma start;
```

To include in your project:
``` sh
bower install --save-dev rumpus-client
```