describe("Rumpus client API", function() {
    var signal,
        Phaser,
        socket,
        options;

    beforeEach(function() {
        signal = {
            dispatch: function () {}
        };

        Phaser = {
            Signal: function () {}
        };

        socket = {
            on: function () {},
            emit: function () {},
            disconnect: function () {}

        };

        options = {
            phaser: Phaser,
            io: function () {
                return socket;
            },
            serverUrl: 'multiplayerServer.rumpus.com:3000',
            server: {
                connections: 2
            },
            user: {
                name: 'bob'
            }
        };

        spyOn(Phaser, 'Signal').and.returnValue(signal);
        spyOn(options, 'io').and.returnValue(socket);
        spyOn(socket, 'on');
        spyOn(socket, 'emit');
        spyOn(socket, 'disconnect');
    });

    it('initialises', function () {
        var rumpusClient = new RumpusClient(options);

        expect(rumpusClient.io).toEqual(options.io);
        expect(rumpusClient.phaser).toEqual(Phaser);
        expect(rumpusClient.serverUrl).toEqual(options.serverUrl);
        expect(rumpusClient.user).toEqual(options.user);

    });

    it('connects', function () {
        var rumpusClient = new RumpusClient(options);

        rumpusClient.connect();

        expect(Phaser.Signal).toHaveBeenCalled();
        expect(options.io).toHaveBeenCalled();
        expect(options.io.calls.argsFor(0)).toEqual(['multiplayerServer.rumpus.com:3000', {connections: 2}]);
    });

    it('exposes phaser signals', function () {
        var rumpusClient = new RumpusClient(options);

        rumpusClient.connect();

        expect(rumpusClient.userProperties).toEqual(signal);
        expect(rumpusClient.userJoin).toEqual(signal);
        expect(rumpusClient.lobbyUsers).toEqual(signal);
        expect(rumpusClient.userLeave).toEqual(signal);
        expect(rumpusClient.userConnect).toEqual(signal);
        expect(rumpusClient.userDisconnect).toEqual(signal);
        expect(rumpusClient.roomFull).toEqual(signal);
        expect(rumpusClient.roomEmpty).toEqual(signal);
    });

    it('listens to custom socket events', function () {
        var rumpusClient = new RumpusClient(options),
            handler = function () {};

        rumpusClient.connect();
        rumpusClient.listenTo('doodah', handler);

        expect(socket.on.calls.argsFor(13)[0]).toEqual('doodah');
    });

    it('disconnects', function () {
        var rumpusClient = new RumpusClient(options);
        rumpusClient.connect();
        rumpusClient.disconnect();

        expect(socket.disconnect).toHaveBeenCalled();
    });
});

